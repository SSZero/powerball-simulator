import random

bucket_whiteball = list(range(1,69))
bucket_redball = list(range(1,26))

class PowerBallPlay:
    numbers = []
    powerball = 0
    prize = None
    def __init__(self, numbers, powerball):
        self.numbers = numbers
        self.powerball = powerball

    def __str__(self):
        padding = [None] * (5-len(self.numbers))
        showprize = len(padding) != 0
        printedNums = list(sorted(self.numbers))
        printedNums += padding
        printedNums.append("({})".format(self.powerball if self.powerball > 0 else None));
        st = ""

        if(showprize):
            st=('\t-\t'.join(('\t'.join('{}'.format(k) for k in printedNums), 'Prize: {}'.format(self.calculatePrize()))))
        else:
            st=('\t'.join('{}'.format(k) for k in printedNums))
        
        return st
    def compare(self, play):
        matchingNumbers = list(set(self.numbers).intersection(play.numbers))
        matchingPowerball = (self.powerball if self.powerball == play.powerball else -1)
        matchedPlay = PowerBallPlay(matchingNumbers, matchingPowerball)
        winnings = matchedPlay.calculatePrize()
        return matchedPlay, winnings
    def calculatePrize(self):
        if(self.prize != None):
            return self.prize;
        else:
            self.prize = 0
            prizesWithoutRedBall = [0, 0, 0, 7, 100, 1000000]
            prizesWithRedBall = [4,4,7,100,50000,800000000]
            numMatches = len(self.numbers)
            
            if(self.powerball > 0):
                self.prize = prizesWithRedBall[numMatches]
            else:
                self.prize = prizesWithoutRedBall[numMatches]
        return self.prize
        
    def print(self, showprize=False):
        padding = [None] * (5-len(self.numbers)) 
        printedNums = list(sorted(self.numbers))
        printedNums += padding
        printedNums.append("({})".format(self.powerball if self.powerball > 0 else None));
        
        if(showprize):
            print('\t-\t'.join(('\t'.join('{}'.format(k) for k in printedNums), 'Prize: {}'.format(self.calculatePrize()))))
        else:
            print('\t'.join('{}'.format(k) for k in printedNums))
    

def getPowerBallNumbers(amountToSpend):
    allPlays = []
    for x in range(0, int(amountToSpend/2)):
        numbers = []
        numbers += sorted(random.sample(bucket_whiteball, 5))
        powerball = random.sample(bucket_redball, 1)[0]
        play = PowerBallPlay(numbers, powerball)
        allPlays.append(play)
        
    return allPlays

def determineWinnings(allPlays, drawnPlays):
    winningPlays = []
    winningAmounts = []
    gen = ((n, d) for n in allPlays for d in drawnPlays)
    for n, d in gen:
        match, winnings = n.compare(d)
        winningPlays.append(match)
        winningAmounts.append(winnings)
    return winningPlays, winningAmounts


if __name__ == "__main__":

    amount = int(input("Enter amount of money to spend: "));

    numbers = getPowerBallNumbers(amount)
    print("Your quick picks:\n=======================")
    print("\n".join(str(n) for n in numbers))


    draws = int(input("\nEnter number of draws: "))
    drawnNumbers = getPowerBallNumbers(draws*2)

    print("\nDrawn Numbers:\n=======================")
    print("\n".join(str(d) for d in drawnNumbers))


    winningPicks, winningAmounts = determineWinnings(numbers, drawnNumbers);

    print("\nWinning Numbers:\n=======================")

    total = 0
    print("\n".join(str(w) for w in winningPicks if w.prize > 0))
    total = sum(int(w) for w in winningAmounts)

    print("\nTotal Won: {}".format(total))

